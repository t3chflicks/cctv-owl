# Smart CCTV Owl 
A T3chFlicks halloween special project.
* Checkout the instructions and fun video [youtube](https://youtu.be/aLX4btGs_x8)
 [![YOUTUBE_VIDEO](https://img.youtube.com/vi/aLX4btGs_x8/0.jpg)](https://www.youtube.com/watch?v=aLX4btGs_x8)
* Checkout the instructional blog post [instructables](https://www.instructables.com/id/Smart-Security-Owl/)

## Code
### Tutorial
1. Move the owl head.
2. Make the owl hoot!
3. Stream the video feed.
4. Run object detection on the stream.
5. Notify your phone.

### Src
This is the complete working version of the code used.

### Assets
This contains the extra files such as the owl hoot and deep learning models.
